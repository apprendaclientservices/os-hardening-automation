# Ansible Scripts for Hardening Windows File System Permissions

### Configure Control Machine:
1. Configure Windows Module
    * On a Linux control machine:
    ```pip install "pywinrm>=0.2.2"```
    * On a Windows control machine use the following [instructions](http://docs.ansible.com/ansible/latest/intro_windows.html#using-a-windows-control-machine)
2. Add your Windows Credentials to the [windows.yaml](./group_vars/windows.yaml)
3. Add your Windows target machine hostname (or IP) to the [hosts](hosts) file

### Configure Target Windows Machine
1. Download [ConfigureRemotingForAnsible.ps1](https://github.com/ansible/ansible/blob/devel/examples/scripts/ConfigureRemotingForAnsible.ps1)

2. Execute the following powershell commmand to enable the "winrm" python module to talk to this remote host:
```powershell -File ConfigureRemotingForAnsible.ps1 -CertValidity 100 -Verbose```

3. To test your connection:
```ansible windows [-i ./path/to/your/hosts] -m win_ping```

4. Execute your playbook:
```ansible-playbook [-i ./path/to/your/hosts] apprenda_hardening.yaml```